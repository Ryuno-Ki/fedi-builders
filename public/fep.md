---
layout: fep.njk
---

# FEP crafter

This page will help you craft a [Fediverse Enhancement Proposal][fep].

<p id="loading">Please wait a moment while we prepare the template</p>

<label for="subject">What do you want to write about?</label>
<input type="text" id="subject" disabled />

<label for="summary">Please summarise your idea</label>
<textarea id="summary" rows="40" disabled></textarea>

Satisfied with your result?
Then follow these steps:

<ol id="steps"></ol>

[fep]: https://codeberg.org/fediverse/fep
