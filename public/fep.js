window.fep = (function () {
  "use strict";
  var encoder, formatTemplate, getShortcode, getTemplate;

  if (
    window.fetch &&
    window.TextEncoder &&
    window.crypto &&
    window.crypto.subtle &&
    window.crypto.subtle.digest &&
    window.Uint8Array &&
    Array.from
  ) {
    encoder = new TextEncoder();

    formatTemplate = function (template, shortCode, subject) {
      return template
        .split("\n")
        .map(function (line) {
          if (/^slug:/.test(line)) {
            return 'slug: "' + shortCode + '"';
          }

          if (/^# FEP-/.test(line)) {
            return "# FEP-" + shortCode + ": " + subject;
          }

          return line;
        })
        .join("\n");
    };

    getShortcode = function (subject) {
      var buf;

      buf = encoder.encode(subject);
      return crypto.subtle
        .digest("SHA-256", buf)
        .then(function (hashAsArrayBuffer) {
          var uint8ViewOfHash;

          uint8ViewOfHash = new Uint8Array(hashAsArrayBuffer);
          return Array.from(uint8ViewOfHash)
            .map(function (byte) {
              return byte.toString(16).padStart(2, "0");
            })
            .join("");
        })
        .then(function (hashAsString) {
          return hashAsString.slice(0, 4);
        });
    };

    getTemplate = function () {
      return fetch("/template.txt").then(function (response) {
        return response.text();
      });
    };
  }

  return {
    format: formatTemplate,
    shortCode: getShortcode,
    template: getTemplate,
  };
})();
