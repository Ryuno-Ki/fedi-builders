const { join } = require("path");

const inputDir = "./public";

function licenseProgrammingLanguagePairs(value) {
  const pairs = new Map();
  value.forEach((app) => {
    if (!app.license) {
      return;
    }

    if (!pairs.has(app.license)) {
      pairs.set(app.license, new Set([app.programmingLanguage]));
    } else {
      const map = pairs.get(app.license);
      map.add(app.programmingLanguage);
      pairs.set(app.license, map);
    }
  });
  return Array.from(pairs).map(([license, programmingLanguages]) => [
    license,
    Array.from(programmingLanguages),
  ]);
}

const copies = ["style.css", "fep.js", "template.txt"];
const filters = [
  {
    name: "licenseProgrammingLanguagePairs",
    value: licenseProgrammingLanguagePairs,
  },
];

module.exports = function (eleventyConfig) {
  copies.forEach((copy) => {
    eleventyConfig.addPassthroughCopy(join(inputDir, copy));
  });

  filters.forEach((filter) => {
    eleventyConfig.addNunjucksFilter(filter.name, filter.value);
  });

  return {
    dir: {
      input: inputDir,
      layouts: "./_layouts",
    },
    dataTemplateEngine: "njk",
    htmlTemplateEngine: "njk",
    markdownTemplateEngine: "njk",
  };
};
