# fedi.builders

Landing page to highlight projects working on federation that are welcoming helping hands based on your skills.

Parses [delightful-fediverse-apps](https://codeberg.org/fediverse/delightful-fediverse-apps)
for its content.
