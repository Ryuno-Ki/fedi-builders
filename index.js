const { writeFile } = require("node:fs/promises");
const { join } = require("node:path");

const fetch = (...args) =>
  import("node-fetch").then(({ default: fetch }) => fetch(...args));
const cheerio = require("cheerio");
const MarkdownIt = require("markdown-it");

const md = new MarkdownIt();
const delightfulFediverseAppsUrl =
  "https://codeberg.org/api/v1/repos/fediverse/delightful-fediverse-apps/contents/README.md";
const fepTemplateUrl =
  "https://codeberg.org/api/v1/repos/fediverse/fep/contents/fep-xxxx-template.md";
const appsPath = join(".", "public", "_data", "apps.json");
const templatePath = join(".", "public", "template.txt");

async function run(url) {
  console.debug("Querying Codeberg");
  const response = await fetch(url);
  const readme = await response.json();
  console.debug("Received response");

  return readme.content;
}

function decode(base64) {
  const buff = Buffer.from(base64, "base64");
  const str = buff.toString("utf-8");

  return str;
}

function parseAsHtml(markdown) {
  // Both, Gitea and Eleventy use CommonMark flavour \o/
  const html = md.render(markdown);

  return html;
}

function parseAsMarkdown(content) {
  return decode(content);
}

function filterForApps(html) {
  const apps = [];
  const $ = cheerio.load(html);
  $("h4 + ul")
    .find("p")
    // Drop unmaintained ones
    .filter((_, el) => !$(el).text().includes(":ghost:"))
    //.map((_, el) => $(el).text())
    .toArray()
    .forEach((app) => {
      const code = $(app).find("code").text();
      const [license, programmingLanguage] = code.split(",");
      const link = $(app).find("a").first().attr("href");
      const name = $(app).find("a").first().text().trim();

      $(app).find("a").remove();
      $(app).find("code").remove();
      const text = $(app)
        .text()
        .replace(":heavy_check_mark:", "")
        .replace(":tada:", "")
        .trim();
      // Start with ': ' after the app name and links
      const description = text.slice(text.indexOf(":") + 2);

      apps.push({
        description,
        license: license.trim(),
        link,
        name,
        programmingLanguage: programmingLanguage
          ? programmingLanguage.trim()
          : "unknown",
      });
    });

  return apps;
}

Promise.all([
  /*
  run(delightfulFediverseAppsUrl)
    .then((content) => {
			const markdown = parseAsMarkdown(content);
      const html = parseAsHtml(markdown);
      const apps = filterForApps(html);
  
      return apps;
    })
    .then((apps) => {
      writeFile(appsPath, JSON.stringify(apps, null, 2), { encoding: "utf8" });
		}),
	*/
  run(fepTemplateUrl)
    .then((content) => parseAsMarkdown(content))
    .then((markdown) => {
      writeFile(templatePath, markdown, { encoding: "utf8" });
    }),
]);
